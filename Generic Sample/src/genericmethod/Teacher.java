package genericmethod;

import java.util.Objects;

public class Teacher extends Person {
    private int id;

    public Teacher(String name, int age, int id) {
        super(name, age);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;
        Teacher teacher = (Teacher) o;
        return getId() == teacher.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
