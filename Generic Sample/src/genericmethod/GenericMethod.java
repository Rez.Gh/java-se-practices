package genericmethod;

public class GenericMethod {

    public static <T extends Person> boolean isEqual(GenericType type1 , GenericType type2){
        return type1.equals(type2);
    }
}
