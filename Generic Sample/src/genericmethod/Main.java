package genericmethod;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Omid", 20, 981);
        Student student2 = new Student("Sara", 20, 982);
        Teacher teacher = new Teacher("Omid",20,981);
        GenericType<Student>  studentGenericType = new GenericType<>(student1);
        GenericType<Student> studentGenericType1 = new GenericType<>(student2);
        GenericType<Teacher>teacherGenericType = new GenericType<>(teacher);
        boolean isEqual = GenericMethod.isEqual(studentGenericType,studentGenericType1);
        System.out.println("Student1 and Student2 equal ? " + isEqual);

        System.out.println("Student1 and Teacher equal ? " + GenericMethod.isEqual(studentGenericType,teacherGenericType));
    }
}
