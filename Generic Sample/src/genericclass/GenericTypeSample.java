package genericclass;

public class GenericTypeSample<T> {
    private T t;

    public GenericTypeSample(T t) {
        this.t = t;
    }

    public T getT() {
        return t;
    }

    @Override
    public String toString() {
        return t.toString();
    }
}
