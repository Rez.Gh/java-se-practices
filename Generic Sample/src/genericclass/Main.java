package genericclass;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Omid", 20, 984332);
        GenericTypeSample<Person> typeSample = new GenericTypeSample<>(student);
        Person student1 = typeSample.getT();
        System.out.println(student1);
    }
}
