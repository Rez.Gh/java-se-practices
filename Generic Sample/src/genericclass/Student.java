package genericclass;

public class Student extends Person {
    private int id;

    public Student(String name, int age, int id) {
        super(name, age);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
      return super.toString() + String.format("Id : %s%n",getId());
    }
}

