package card_checker;

import dao.PreNumberCreditCards;

import java.util.HashMap;

public final class  CardCheckerUtility {
    private  CardCheckerUtility() {
    }

    public final static void cardChecker(String inputUserCard){
        String creditCardNumber = inputUserCard;
        HashMap<String,String> preNumberBankcard = PreNumberCreditCards.getPreNumberCreditCards();
        String[] numbersCard = new String [inputUserCard.length()];
        numbersCard = inputUserCard.split("-");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < numbersCard.length ; i++) {
            builder.append(numbersCard[i]);
        }
        inputUserCard = builder.toString();
        inputUserCard = inputUserCard.substring(0,6);

        for (int i = 0; i < preNumberBankcard.size(); i++) {
            if (preNumberBankcard.containsKey(inputUserCard)){
                System.out.printf("Credit card : %s%n", creditCardNumber);
                System.out.printf("Bank Name : %s%n", preNumberBankcard.get(inputUserCard));
                System.out.printf("Pre number credit card of this Bank : %s%n", inputUserCard);
                break;
            }
        }
    }
}
