package main;

import card_checker.CardCheckerUtility;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter Your credit card number : ");
        String inputUserCard  = new Scanner(System.in).nextLine();
        CardCheckerUtility.cardChecker(inputUserCard);
    }
}
