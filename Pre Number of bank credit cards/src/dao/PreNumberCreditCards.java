package dao;

import java.io.*;
import java.util.HashMap;

public final class PreNumberCreditCards {

    private PreNumberCreditCards() {
    }

    public static final HashMap<String,String> getPreNumberCreditCards() {
         String preNumberCreditCards;
         String bankName;
        HashMap<String, String> preNumberBankcard= new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("PreNumberCreditCards.txt"))) {

            while (reader.ready()) {
                preNumberCreditCards = reader.readLine();
                bankName = reader.readLine();
                preNumberBankcard.put(preNumberCreditCards, bankName);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return preNumberBankcard;
    }
}
