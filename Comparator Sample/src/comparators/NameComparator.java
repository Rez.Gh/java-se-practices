package comparators;

import model.Student;

import java.util.Comparator;

public class NameComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        if(o1.getName().compareTo(o2.getName()) >= 1){
            return 1;
        }else  if(o1.getName().compareTo(o2.getName()) <= -1){
            return -1;
        }
        return 0;
    }
}
