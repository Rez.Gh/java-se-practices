package main;

import comparators.GradeComparator;
import comparators.NameComparator;
import model.Student;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("Setareh", 12, 100));
        studentList.add(new Student("Reza", 12, 80));
        studentList.add(new Student("Kosar", 12, 90));
        studentList.add(new Student("Omid", 12, 50));
        studentList.add(new Student("Negar", 12, 88));
        studentList.add(new Student("Roya", 12, 100));

        System.out.println("Before Grade sorting : ");
        studentList.forEach(System.out::println);
        System.out.println("---------------------------------------");

        GradeComparator gradeComparator = new GradeComparator();
        studentList.sort(gradeComparator);

        System.out.println("After Grade sorting : ");
        studentList.forEach(System.out::println);
        System.out.println("---------------------------------------");

        System.out.println("Before Name sorting : ");
        studentList.forEach(System.out::println);
        System.out.println("---------------------------------------");

        NameComparator nameComparator = new NameComparator();
        studentList.sort(nameComparator);

        System.out.println("After Name sorting : ");
        studentList.forEach(System.out::println);
        System.out.println("---------------------------------------");

    }
}

