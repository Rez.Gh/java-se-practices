package parkingservice;

import exceptions.ParkingClosed;
import model.Car;
import model.ParkingAgent;
import model.Receipt;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;


public class Parking {
    private Car car;
    private ParkingAgent parkingAgent;
    private Receipt receipt;
    private static final double FEE = 1000.0;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public ParkingAgent getParkingAgent() {
        return parkingAgent;
    }

    public ParkingAgent creatGuard() throws ParkingClosed {
        LocalTime currentTime = LocalTime.now();
        if (currentTime.isAfter(LocalTime.parse("00:00", DateTimeFormatter.ofPattern("HH:mm")))
                && currentTime.isBefore(LocalTime.parse("08:00", DateTimeFormatter.ofPattern("HH:mm")))) {
            throw new ParkingClosed();

        } else if (currentTime.isAfter(LocalTime.parse("08:00", DateTimeFormatter.ofPattern("HH:mm")))
                && currentTime.isBefore(LocalTime.parse("14:00", DateTimeFormatter.ofPattern("HH:mm")))) {
            this.parkingAgent = new ParkingAgent("Saman");
        } else if (currentTime.isAfter(LocalTime.parse("14:00", DateTimeFormatter.ofPattern("HH:mm")))
                && currentTime.isBefore(LocalTime.parse("23:59", DateTimeFormatter.ofPattern("HH:mm")))) {
            this.parkingAgent = new ParkingAgent("Iman");
        }
        return this.parkingAgent;
    }

    public Receipt getReceipt() {

        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public static double getFEE() {
        return FEE;
    }

    public void parkingService() {
        System.out.println("Parking App");
        try {
            if (creatGuard() != null) {
                System.out.println("Create Receipt :");
                System.out.println("Enter Car model : ");
                String carName = new Scanner(System.in).nextLine();
                System.out.println("Enter Car Id : ");
                String carId = new Scanner(System.in).nextLine();
                System.out.println("Enter Car color : ");
                String carColor = new Scanner(System.in).nextLine();
                setCar(new Car(carName, carId, carColor));
                LocalTime enterTime = LocalTime.parse(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
                System.out.println("Enter time of exit (HH:mm) : ");
                String exitTimeString = new Scanner(System.in).nextLine();
                LocalTime exitTime = LocalTime.parse(exitTimeString, DateTimeFormatter.ofPattern("HH:mm"));
                double cost = 0;
                try {
                     cost = calculateCost(enterTime, exitTime);
                }catch (ParkingClosed parkingClosed){
                    parkingClosed.getMessage();
                }

                setReceipt(new Receipt(car, creatGuard(), enterTime, exitTime, cost));
                System.out.println(getReceipt());
            } else {
                System.out.println("Between clocks 00:00 and 08:00 Parking closed !");
            }
        } catch (ParkingClosed parkingClosed) {
            parkingClosed.getMessage();
        }
    }

    private double calculateCost(LocalTime startTime, LocalTime endTime) throws ParkingClosed {
        double cost = 0;
        if (!(endTime.isAfter(LocalTime.parse("00:00", DateTimeFormatter.ofPattern("HH:mm")))
                && endTime.isBefore(LocalTime.parse("08:00", DateTimeFormatter.ofPattern("HH:mm"))))) {
            double a = endTime.toSecondOfDay() / 60d / 60d;
            double b = startTime.toSecondOfDay() / 60d / 60d;
            double parkedTimeHour = a - b;
           return cost = FEE * parkedTimeHour;
        } else {
            throw new ParkingClosed();
        }
    }
}
