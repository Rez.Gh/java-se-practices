package model;

public class Car {
    private String carId;
    private String carName;
    private String color;
    private double budget;

    public Car( String carName,String carId, String color) {
        this.carName = carName;
        this.carId = carId;
        this.color = color;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    @Override
    public String toString() {
       return String.format("Name : %s%nCar Id : %s%nColor : %s%n",carName,carId,carName);
    }
}
