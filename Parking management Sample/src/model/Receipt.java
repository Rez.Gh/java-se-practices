package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


public class Receipt {
    private Car car;
    private ParkingAgent parkingAgent;
    private LocalTime enterTime;
    private LocalTime exitTime;
    private double cost;
    private LocalDate receiptDate;

    public Receipt(Car car, ParkingAgent parkingAgent, LocalTime enterTime, LocalTime exitTime, double cost) {
        this.car = car;
        this.parkingAgent = parkingAgent;
        this.enterTime = enterTime;
        this.exitTime = exitTime;
        this.cost = cost;
        receiptDate = LocalDate.now();
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public ParkingAgent getParkingAgent() {
        return parkingAgent;
    }

    public void setParkingAgent(ParkingAgent parkingAgent) {
        this.parkingAgent = parkingAgent;
    }

    public LocalTime getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(LocalTime enterTime) {
        this.enterTime = enterTime;
    }

    public LocalTime getExitTime() {
        return exitTime;
    }

    public void setExitTime(LocalTime exitTime) {
        this.exitTime = exitTime;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }


    @Override
    public String toString() {
        return String.format("Date : %s%nCar Name : %s%nCar Id : %s%nParking agent name : %s%nTime to enter the parking : " +
                        "%s%nTime to leave the parking : %s%nCost : %.2f $%n",
                receiptDate,car.getCarName(),car.getCarId(), parkingAgent.getName(), enterTime, exitTime,cost);
    }
}
