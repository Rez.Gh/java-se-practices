package model;

import java.time.LocalTime;

public class ParkingAgent {

    private String name;
    private LocalTime startShiftWork;
    private LocalTime endShiftWork;

    public ParkingAgent(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getStartShiftWork() {
        return startShiftWork;
    }

    public void setStartShiftWork(LocalTime startShiftWork) {
        this.startShiftWork = startShiftWork;
    }

    public LocalTime getEndShiftWork() {
        return endShiftWork;
    }

    public void setEndShiftWork(LocalTime endShiftWork) {
        this.endShiftWork = endShiftWork;
    }

    @Override
    public String toString() {
       return String.format("Agent name : %s%nStart work : %s%nEnd work : %s%n",name,startShiftWork,endShiftWork);
    }
}
