package exceptions;

public class ParkingClosed extends Throwable {


    public ParkingClosed() {
    }


    @Override
    public String getMessage() {
        return "Between clocks 00:00 and 08:00 Parking closed !";
    }
}
