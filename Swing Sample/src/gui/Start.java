package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.SwingConstants.CENTER;


public class Start extends JFrame {
    private JPanel mainPanel;
    private JLabel title;
    private JTextField inputName;
    private JButton enterBtn;
    private JLabel text;
    private String name;

    public Start() throws HeadlessException {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(400, 200));
        setSize(new Dimension(400, 200));
        setComponentOrientation(getComponentOrientation());
        setLayout(new GridLayout());

        mainPanel = new JPanel();
        mainPanel.setAlignmentX(90);
        mainPanel.setAlignmentY(90);
        mainPanel.setSize(400, 200);
        mainPanel.setBackground(Color.white);
        title = new JLabel();
        title.setText("Enter your name : ");
        title.setHorizontalAlignment(CENTER);
        title.setFont(new Font("Titillium Web", 1, 24));
        title.setSize(20, 20);
        inputName = new JTextField();
        inputName.setHorizontalAlignment(CENTER);
        inputName.setFont(new Font("Titillium Web", 1, 14));
        inputName.setPreferredSize(new Dimension(200, 30));
        enterBtn = new JButton();
        enterBtn.setHorizontalAlignment(CENTER);
        enterBtn.setText("Click !");
        enterBtn.setBackground(Color.BLACK);
        enterBtn.setForeground(Color.white);
        text = new JLabel();
        text.setVisible(false);
        enterBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                name = inputName.getText();
                text.setVisible(true);
                inputName.setText("");
                text.setText("Hello " + name);
                text.setHorizontalAlignment(CENTER);
                text.setFont(new Font("Titillium Web", 1, 24));
            }
        });
        mainPanel.add(title);
        mainPanel.add(inputName);
        mainPanel.add(enterBtn);
        mainPanel.add(text);
        add(mainPanel);
        mainPanel.setVisible(true);

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
