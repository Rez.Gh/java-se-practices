package textfile_io;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("text.txt"))){
            System.out.println("Type :");
            String input = new Scanner(System.in).nextLine();
            writer.write(input);
            System.out.println("------------------------------");
            System.out.println("Text write in text.txt file !");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try(BufferedReader reader = new BufferedReader(new FileReader("text.txt"))) {
            String output = null;
            while (reader.ready()){
                output = reader.readLine();
            }
            System.out.println("Text read from text.txt file !");
            System.out.println("------------------------------");
            System.out.println(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
