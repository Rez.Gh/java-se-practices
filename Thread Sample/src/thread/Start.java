package thread;

public class Start {

    public static void main(String[] args) {
        System.out.println("Main Thread !");
        System.out.println("----------------------------------------");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread nameThread = new Thread(new PrintNameTask(), "NameTask");
        Thread ageThread = new Thread(new PrintAgeTask(), "AgeTask");

        try {
            nameThread.start();
            nameThread.join();
            ageThread.start();

        } catch (InterruptedException e) {
            e.getStackTrace();
        }
    }
}
