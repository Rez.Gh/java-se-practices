package thread;

import java.util.ArrayList;
import java.util.List;

public class PrintNameTask implements Runnable {
    @Override
    public void run() {

        List<String> names = new ArrayList<>();
        names.add("Reza");
        names.add("Omid");
        names.add("Setareh");
        names.add("Negar");
        for (String name : names) {
            try {
                System.out.println("Name: " + name);
                System.out.println("----------------------------------------");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
