package thread;

import java.util.ArrayList;
import java.util.List;

public class PrintAgeTask implements Runnable {
    @Override
    public void run() {

        List<Integer> Ages = new ArrayList<>();
        Ages.add(28);
        Ages.add(31);
        Ages.add(24);
        Ages.add(25);
        for (Integer age : Ages) {
            try {
                System.out.println("Age :" + age);
                System.out.println("----------------------------------------");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
