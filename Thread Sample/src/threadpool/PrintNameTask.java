package threadpool;

import java.util.ArrayList;
import java.util.List;

public class PrintNameTask implements Runnable {
    @Override
    public void run() {
        List<String> names = new ArrayList<>();
        names.add("Reza");
        names.add("Omid");
        names.add("Setareh");
        names.add("Negar");
        for (String name : names){
            System.out.println("Name: " +name);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
