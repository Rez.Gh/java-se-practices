package threadpool;

import java.util.ArrayList;
import java.util.List;

public class PrintAgeTask implements Runnable {
    @Override
    public void run() {
        List<Integer> Ages = new ArrayList<>();
        Ages.add(28);
        Ages.add(31);
        Ages.add(24);
        Ages.add(25);
        for (Integer age : Ages) {
            System.out.println("Age :" + age);
            System.out.println("----------------------------------------");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
