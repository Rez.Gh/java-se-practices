package main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Enter Initial Wallet Balance :");
        double initial = new Scanner(System.in).nextDouble();
        Wallet wallet = new Wallet(initial);
        System.out.println("Enter amount of decrement : ");
        double decrement = -(new Scanner(System.in).nextDouble());
        System.out.println("Enter amount of increment : ");
        double increment = new Scanner(System.in).nextDouble();
        Thread changeBalanceThread1 = new Thread(new ChangeBalanceThread(wallet, decrement));
        Thread changeBalanceThread2 = new Thread(new ChangeBalanceThread(wallet, increment));
        changeBalanceThread1.start();
        changeBalanceThread2.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.getStackTrace();
        }

        System.out.println(wallet);
    }
}
