package main;

public class Wallet {
    private double moneyBalance;

    public void setMoneyBalance(double moneyBalance) {
        this.moneyBalance = moneyBalance;
    }

    public Wallet(double moneyBalance) {

        this.moneyBalance = moneyBalance;
    }

    private double getMoneyBalance() {
        return moneyBalance;
    }

    public synchronized void changeBalanceMoney(double amount) {

        if (amount < 0) {
            amount = Math.abs(amount);
            moneyBalance -= amount;
            return;
        } else if (amount > 0) {
            moneyBalance += amount;
        }
    }

    @Override
    public String toString() {
        return String.format("Wallet Balance : %s", getMoneyBalance());
    }

    public void printBalanceMoney() {
        System.out.println(this.toString());
    }
}
