package main;

public class ChangeBalanceThread implements Runnable {
    private Wallet wallet;
    private double amount;

    public ChangeBalanceThread(Wallet wallet, double amount) {
        this.wallet = wallet;
        this.amount = amount;
    }

    @Override
    public void run() {

        wallet.changeBalanceMoney(amount);
    }
}
