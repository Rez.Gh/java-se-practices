package simple_game;


import java.util.Random;
import java.util.Scanner;

public class SimpleGame {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome !");
        System.out.println("Press a key to throw the dice ");
        scanner.next();
        System.out.println("You throw the dice ");
        System.out.println("Computer Throw the dice");
        System.out.println("How many time Play :");
        int gameCount = scanner.nextInt();
        int myWins = 0;
        int comWins = 0;
        int equal = 0;
        Random rand = new Random();
        for (int i = 0; i < gameCount; i++) {
            int myNumber = rand.nextInt(6) + 1;
            int comNumber = rand.nextInt(6) + 1;
            if (myNumber > comNumber) {
                myWins++;
            } else if (myNumber < comNumber) {
                comWins++;
            } else {
                equal++;
            }
        }
        System.out.println("Your Wins : " + myWins);
        System.out.println("Computer Wins : " + comWins);
        System.out.println("Equal :" + equal);
        if (myWins > comWins) {
            System.out.println("You Win !");
        } else if (myWins < comWins) {
            System.out.println("You lose !");
        } else {
            System.out.println("Equal !");
        }
    }
}
